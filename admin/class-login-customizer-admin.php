<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Login_Customizer
 * @subpackage Login_Customizer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Login_Customizer
 * @subpackage Login_Customizer/admin
 * @author     Your Name <email@example.com>
 */
class Login_Customizer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Login_Customizer    The ID of this plugin.
	 */
	private $Login_Customizer;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Login_Customizer       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Login_Customizer, $version ) {

		$this->Login_Customizer = $Login_Customizer;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Login_Customizer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Login_Customizer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Login_Customizer, plugin_dir_url( __FILE__ ) . 'css/login-customizer-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Login_Customizer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Login_Customizer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->Login_Customizer, plugin_dir_url( __FILE__ ) . 'js/login-customizer-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Adds Customizer section and controls
	 *
	 * @since  1.0.0
	 *
	 * Added by Travis Johnson
	 */
	function login_customize_register($wp_customize) {
		$wp_customize->add_section(
			'theme_login',
			array(
				'title' => __('Login', 'login-customizer'),
				'priority' => 90,
			)
		);

		$wp_customize->add_setting(
			'login_logo',
			array(
				'default' => '',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
			$wp_customize,
			'login_logo',
			array(
				'label'      => __( 'Upload a login logo', 'login-customizer' ),
				'section'    => 'theme_login',
				'settings'   => 'login_logo',
				'context'    => 'your_setting_context' 
			)
		)
		);

		$wp_customize->add_setting(
			'login_bg_image',
			array(
				'default' => '',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
			$wp_customize,
			'login_bg_image',
			array(
				'label'      => __( 'Upload a login background', 'login-customizer' ),
				'section'    => 'theme_login',
				'settings'   => 'login_bg_image',
				'description'    => __( 'Set the background image of the login screen. Use an image that is at least 1200px X 1200px.', 'login-customizer' ) 
			)
		)
		);

		$wp_customize->add_setting(
			'login_color',
			array(
				'default' => '',
			)
		);
		$wp_customize->add_control(
			'login_color',
			array(
				'label' => __('Login Link and Button Color', 'login-customizer'),
				'section' => 'theme_login',
				'type' => 'text',
				'description'    => __( 'Select a color or enter a hex value. Ex: #FFFFFF .', 'login-customizer' ) 
			)
		);
		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
			$wp_customize, 
			'login_color', 
			array(
				'label'      => __('Login Link and Button Color', 'login-customizer'),
				'section'    => 'theme_login',
				'settings'   => 'login_color',
				'description'    => __( 'Enter a hex or rgba value. Ex: #FFFFFF or rgba(255,255,255,1).', 'login-customizer' ) 
			) ) 
		);

		$wp_customize->add_setting(
			'login_form_theme',
			array(
				'default' => '',
			)
		);
		$wp_customize->add_control(
			'login_form_theme',
			array(
				'label' => __('Login Theme', 'login-customizer'),
				'section' => 'theme_login',
				'type' => 'radio',
				'choices'=> array('light-theme' => 'Light', 'dark-theme' => 'Dark'),
				'description'    => __( 'Choose a light or dark theme.', 'login-customizer' ) 
			)
		);
	}

	/**
	 * Adds Custom Login styles
	 * @since  1.0.0
	 *
	 * Added by Travis Johnson
	 */
	function visceral_custom_login_style() {
		// Set login color; default is WordPress blue
		$login_color = '#0085ba';
		if ( get_theme_mod( 'login_color' ) )  {
			$login_color = get_theme_mod( 'login_color' );
		} 

		// Set button style only if a custom color is chosen
		if ( get_theme_mod( 'login_color' ) )  {
			$login_button_style = '.visc-login-inner input.button {
			width: 100%;
			height: inherit !important;
			padding: 16px 22px !important;
			margin-bottom: 20px;
			border-radius: 0;
			font-size: 12px;
			line-height: 18px !important;
			text-transform: uppercase;
			letter-spacing: 1px;
			font-weight: bold;
			background: ' . $login_color . ' !important;
			border-color: ' . $login_color . ' !important;
			box-shadow: none !important;
			color: #fff;
			text-decoration: none !important;
			text-shadow: none !important;
			transition: all 0.3s;
			}

			.visc-login-inner input.button:hover, .visc-login-inner input.button:focus {
			background: ' . $login_color . ' !important;
			border-color: ' . $login_color . ' !important;
			opacity: 0.8;
			}';
		}  
		$style_output = '<style type="text/css">';
		// Change login logo
		if ( get_theme_mod( 'login_logo' ) )  {
			list($width,$height) = getimagesize(get_theme_mod( "login_logo" ));
			
			$style_output .= 'h1 a {
				background-image:url('. get_theme_mod( "login_logo" ) .') !important;
				background-size: '. $width .'px !important;
				width: '. $width .'px !important;
				height: '. $height .'px !important;
			}';
		}
		// Change login background image
		if ( get_theme_mod( 'login_bg_image' ) )  {    
			$style_output .= 'body {
				background-image:url('. get_theme_mod( "login_bg_image" ) .');
				background-repeat: no-repeat;
				background-size: cover;
				background-position: center;
			}';
		}

		$style_output .= '#login {
			width: 425px;
		}

		.visc-login-inner {
			background: rgba(255, 255, 255, .8);
			padding: 50px;
		}

		.login label {
			color: black;
		}

		.visc-login-inner form {
			margin-top: 40px;
			padding: 0;
			background: none;
			box-shadow: none;
		}

		.visc-login-inner form label span {
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute !important;
			height: 1px;
			width: 1px;
			overflow: hidden;
		}

		.visc-login-inner form input[type="text"], .visc-login-inner form input[type="password"] {
			border: 1px solid #d5d5d6 !important;
			border-radius: 1px !important;
			background: rgba(255, 255, 255, .4);;
			box-shadow: none !important;
			padding: 17px !important;
			margin-bottom: 0 !important;
			font-size: 16px;
			color: #000000;
			letter-spacing: 0;
		}

		.visc-login-inner form input[type="text"]:focus, .visc-login-inner form input[type="password"]:focus {
			border-color: ' . $login_color . ' !important;
			outline: none;
			box-shadow: none;
		}

		.visc-login-inner form input#user_pass {
			margin-bottom: 17px !important;
		}

		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
			color: black;
		}
		::-moz-placeholder { /* Firefox 19+ */
			color: black;
		}
		:-ms-input-placeholder { /* IE 10+ */
			color: black;
		}
		:-moz-placeholder { /* Firefox 18- */
			color: black;
		}

		input[type=checkbox]:checked:before {
			color: black !important;
		}

		input[type=checkbox]:focus {
			box-shadow: none !important;
			border-color: ' . $login_color . ' !important;
		}

		'
		. $login_button_style .
		'

		.login form .forgetmenot {
			margin-top: 0 !important;
			margin-bottom: 17px !important;
		}

		.login #backtoblog, .login #nav, #visceral-brand {
			text-align: center;
			font-size: 16px;
		}

		.login #backtoblog {
			margin-top: 10px !important;
		}

		.login #backtoblog a, .login #nav a, #login_error a {
			color: ' . $login_color . ';
		}

		.login #backtoblog a:hover, .login #nav a:hover, #login_error a:hover, .login #backtoblog a:focus, .login #nav a:focus, #login_error a:focus  {
			color: ' . $login_color . ';
			opacity: 0.8;
		}

		#visceral-brand {
			margin-top: 50px;
			color: black;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		#visceral-brand a {
			text-decoration: none;
			color: ' . $login_color . ';
		}

		#visceral-brand a:hover, #visceral-brand a:focus {
			color: ' . $login_color . ';
			opacity: 0.8;
		}

		#visceral-brand img {
			transition: all 0.3s;
		}

		#visceral-brand img:hover, #visceral-brand img:focus {
			opacity: 0.8;
		}
		
		#visceral-brand a#brand-img {
			background-image: url(' . plugins_url( 'images/visceral-logo.svg', __FILE__ )  . ');
			background-repeat: no-repeat;
			width: 50px;
			height: 34px;
			margin-right: 18px; 
			transition: all 0.3s; 
		}

		#lostpasswordform #wp-submit {
			margin-top: 17px;
		}

		.login .message, #login_error {
			background: rgba(117, 152, 188, .3) !important;
			border: 0 !important;
			box-shadow: none !important;
		}

		#login_error {
			background: rgba(200, 16, 46, .3) !important;
		}

		.password-input-wrapper {
			width: 100% !important;
			margin-bottom: 17px !important;
		}

		.wp-hide-pw {
			text-align: center !important;
		}

		.dark-theme .visc-login-inner {
			background: rgba(0, 0, 0, .8);
		}

		.login .dark-theme label {
			color: #fff !important;
		}

		.login .dark-theme #backtoblog a, .login .dark-theme #nav a, .dark-theme #login_error a {
			color: #fff;
		}

		.login .dark-theme #backtoblog a:hover, .login .dark-theme #nav a:hover, .dark-theme #login_error a:hover, .login .dark-theme #backtoblog a:focus, .login .dark-theme #nav a:focus, .dark-theme #login_error a:focus  {
			color: #fff;
			opacity: 0.8;
		}

		.dark-theme #visceral-brand {
			margin-top: 50px;
			color: #fff;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.dark-theme #visceral-brand a {
			text-decoration: none;
			color: #fff;
		}

		.dark-theme #visceral-brand a:hover, #visceral-brand a:focus {
			color: #fff;
			opacity: 0.8;
		}

		.dark-theme #visceral-brand a#brand-img {
			background-image: url(' . plugins_url( 'images/visceral-logo-white.svg', __FILE__ )  . ');
		}
		';

		$style_output .= '</style>';

		echo $style_output;
	}
 
	/**
	 * Adds javascript to run on login page
	 * @since  1.0.0
	 *
	 * Added by Travis Johnson
	 */
	function visc_login_scripts() {
		wp_enqueue_script( 'visc-login', get_template_directory_uri() . '/dist/scripts/login.js', array( 'jquery' ), '1.0.0', true );
	}

	/**
	* Changes login theme
	* @since  1.0.0
	*
	* Added by Travis Johnson
	*/
	function visceral_custom_login_script() {
		// If the Theme Customizer settting for login form theme is changed
		if ( get_theme_mod( 'login_form_theme' ) === 'dark-theme') {
			$script_output = '<script type="text/javascript">';
			$script_output .= "jQuery(document).ready(function($){
				$(\"#login\").addClass(\"dark-theme\");
			});";
			$script_output .= '</script>';
			echo $script_output;
		}
	}

	/**
	 * Changes login logo link
	 * @since  1.0.0
	 *
	 * Added by Travis Johnson
	 */
	function visceral_login_logo_url() {
		if ( get_theme_mod( 'login_logo' ) )  {
			return get_bloginfo( 'url' );
		}  
	}

	/**
	 * Changes login logo title
	 * @since  1.0.0
	 *
	 * Added by Travis Johnson
	 */
	function visceral_login_logo_url_title() {
		if ( get_theme_mod( 'login_logo' ) )  {
			return get_bloginfo( 'name' );
		}  
	}
}
